# 14 - Back-end

Apesar de suas diferenças, os desenvolvedores de front-end e back-end se completam na maioria dos casos e, principalmente, quando o assunto é desenvolvimento web. Primeiramente vamos focar no Back-end.

Começando nessa semana, vocês terão até a terça-feira, dia 02 de Julho para nos entregar um projeto final, colocando em prática TUDO (ou quase tudo) que viram nesse curso. Vocês precisarão pensar em um App completo com Front-end e Back-end em perfeita sintonia.

A ideia é começar pelo Back-end. Vamos unir o que foi feito dos desafios anteriores de MongoDB e Express, então vamos criar um Endpoint GET e POST, para a busca e a criação de algo - o que? - Vocês decidem.

Lembrem de tratar os erros que tiverem e devolver uma resposta de erro para a requsição `res.send("Deu erro")`. Em express encontrei muitos erros e fins de fluxo, em que não davam uma resposta para a requisição, então a requisição só terminava no Timeout.

# Desafio

Pensem e planejem o App Final de vocês. Nessa fase inicial, vamos pensar no Back-end, nas coisas que vocês irão precisar para mostrar na tela e também para salvar em seu banco de dados.

### Requisitos do App Final:

- Deve ter um Layout no Front-End, então será cobrado quanto a estética do App:
    - Caso escolha React Native, tudo deve funcionar, tanto no Android quanto num iPhone. 
    - Caso escolha React JS, o layout deve ser responsivo e utilizar de práticas básicas do PWA para salvar o site na tela inicial e ter os icones personalizados. 
- Deve ter pelo menos um Endpoint GET e POST sendo chamado de uma tela.
- Os Endpoints GET e POST devem ser construídos para utilizar o MongoDB.
- Separar o desenvolvimento em 3 branchs do GIT diferentes, `entrega-1`, `entrega-2` e `entrega-3` - Cada um será entregue em seus respectivos repositórios. (para criar entre a entrega 1 e 2 basta usar `git checkout -b entrega-2` estando na branch `entrega-1`, isso replicará o conteúdo da branch)

## Entrega 1

Para a primeira entrega, dia 25 de Junho, vocês devem preparar o ambiente de Back-End seguindo os passos abaixo:

**TODOS DEVEM APRESENTAR A SUA IDEIA VIA SLACK COM OS TÓPICOS ABAIXO RESPONDIDOS ANTES DE COMEÇAREM A CODAR**

1. Qual a sua ideia para o App final?
2. Para o Front-end, o que você usuará, React JS ou React Native?
3. Crie um *pequeno* fluxo de como o seu App funcionará. (Ferramenta para auxilio: Lucidchart)
4. Quantos e quais endpoints você criará?

Após enviado no Slack, marcado para o Gustavo e o Gabriel e o feedback de que esta tudo certo para seguir, vocês deverão:

- Criar um Endpoint GET que servirá para a busca
- Criar um Endpoint POST que servirá para a criação de um registro
- Os dois Endpoints devem fazer uso do MongoDB

### Dicas

- Se PLANEJAR bem é o segredo, façam esboços da tela e criem fluxogramas para definir bem o fluxo do métodos
- PONDEREM quanto ao `console.log()`, é bom utilizar para saber como suas váriaveis estão se comportando, mas não deve-se deixar um `console.log` avulso, torna a leitura do código mais dificil
- VALIDEM conosco no Slack as lógicas que fizerem, desde um fluxo básico a um complexo.
- Exemplos: Calculadora, Páginas relacionadas à ecommerce (página de produto, listagem de pedidos), Agenda, Cópia do Twitter...
